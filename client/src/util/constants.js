let host = '';
if(process.env.NODE_ENV === 'production'){
    host = process.env.REACT_APP_PROD_API_HOST;
} else {
    host = process.env.REACT_APP_DEV_API_HOST;
}

// var hostname = window.location.hostname;
// if(hostname === 'localhost'){
//     host = 'http://localhost:443';
// } else {
//     host = 'http://localhost:443';
// }
let constants = {
    host
}

export default constants;