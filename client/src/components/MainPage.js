import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Home from './Home';
import Jokes from './Jokes';
import Data from './Data';
import Airports from './Airports';

const MainPage = () => {
  return (
    <Switch> {/* The Switch decides which component to show based on the current URL.*/}
      <Route exact path='/' component={Home}></Route>
      <Route exact path='/jokes' component={Jokes}></Route>
      <Route exact path='/data' component={Data}></Route>
      <Route exact path='/airports' component={Airports}></Route>
    </Switch>
  );
}

export default MainPage;