import { Component } from "react";
import { Link } from "react-router-dom";
import axios from 'axios';
import constants from '../util/constants';

class Jokes extends Component {


    state = {
        isFetching: false,
        jokes: []
    };
    
    
    componentDidMount() {
        this.fetchData();
    }

    render() {
        return (
            <div>
                <h1>Jokes</h1>
                <ol>
                    {this.state.jokes.map((joke, index) => {
                        return(
                            <li key={index}>
                                {joke}
                            </li>
                        )
                    })}
                </ol>
                <Link to="/">
                    <button variant="outlined">
                        Homepage
                    </button>
                </Link>
                <Link to="/data">
                    <button variant="outlined">
                        Go to data
                    </button>
                </Link>
                <Link to="/airports">
                    <button variant="outlined">
                        Go to airports
                    </button>
                </Link>
            </div>
        );
      }

      async fetchData() {
        try {
            this.setState({...this.state, isFetching: true});
            const response = await axios.get(constants.host + 'api/jokes');
            this.setState({jokes: response.data.jokes, isFetching: false});
        } catch (e) {
            console.log(e);
            this.setState({...this.state, isFetching: false});
        }
    }
}

export default Jokes;