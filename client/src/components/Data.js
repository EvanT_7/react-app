import { Component } from "react";
import { Link } from "react-router-dom";
import axios from 'axios';
import constants from '../util/constants';

class Data extends Component {

    state = {
        isFetching: false,
        data: ""
    };


    componentDidMount() {

        // if (this.state.data == "") {
            //console.log(this.state.data);
            this.fetchData();
        // }
    }


    render() {
        return (
            <div>
                <h1>Data</h1>
                <h1>{this.state.data}</h1>
                <Link to="/jokes">
                    <button variant="outlined">
                        Jokes
                    </button>
                </Link>
                <Link to="/">
                    <button variant="outlined">
                        Homepage
                    </button>
                </Link>
                <Link to="/airports">
                    <button variant="outlined">
                        go to airports
                    </button>
                </Link>
            </div>
        );
    }

    async fetchData() {
        try {
            this.setState({ ...this.state, isFetching: true });
            const response = await axios.get(constants.host + 'api/data');
            this.setState({ data: response.data.data, isFetching: false });
        } catch (e) {
            console.log(e);
            this.setState({ ...this.state, isFetching: false });
        }
    }
}

export default Data;