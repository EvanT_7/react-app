import { Component } from "react";
import { Link } from "react-router-dom";
import axios from 'axios';
import constants from '../util/constants';

class Airports extends Component {


    state = {
        isFetching: false,
        airports: []
    };
    
    
    componentDidMount() {
        this.fetchData();
    }

    render() {
        return (
            <div>
                <h1>Airports</h1>
                <ul>
                    {this.state.airports.map((airports, index) => {
                        return(
                            <li key={index}>
                                {airports.id + " - " + airports.name + " - " + airports.code}
                            </li>
                        )
                    })}
                </ul>
                <Link to="/">
                    <button variant="outlined">
                        Homepage
                    </button>
                </Link>
                <Link to="/data">
                    <button variant="outlined">
                        Go to data
                    </button>
                </Link>
                <Link to="/jokes">
                    <button variant="outlined">
                        Go to jokes
                    </button>
                </Link>
            </div>
        );
      }

      async fetchData() {
        try {
            this.setState({...this.state, isFetching: true});
            const response = await axios.get(constants.host + 'api/airports');
            this.setState({airports: response.data, isFetching: false});
        } catch (e) {
            console.log(e);
            this.setState({...this.state, isFetching: false});
        }
    }
}

export default Airports;