import { Component } from "react";
import { Link } from "react-router-dom";

class Home extends Component {
    render() {
        return (
            <div>
                <h1>Home</h1>
                <Link to="/jokes">
                    <button variant="outlined">
                        Go to Jokes
                    </button>
                </Link>
                <Link to="/data">
                    <button variant="outlined">
                        Go to Data
                    </button>
                </Link>
                <Link to="/airports">
                    <button variant="outlined">
                        Go to Airports
                    </button>
                </Link>
            </div>
        );
    }
}

export default Home;