"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
// import connection from './config/db-config';
const airports_controller_1 = __importDefault(require("./controllers/airports-controller"));
require('dotenv').config();
const app = (0, express_1.default)();
const port = process.env.SERVER_PORT; // default port to listen
const data = {
    "data": "Data from the server!"
};
const jokes = {
    "jokes": ["Joke1", "Joke2", "Joke3", "Joke4", "Joke5"]
};
// define a route handler for the default home page
app.get("/api", (req, res) => {
    res.send("Home page");
});
app.get("/api/data", (req, res) => {
    res.send(data);
});
app.get("/api/jokes", (req, res) => {
    res.send(jokes);
});
const airportsController = new airports_controller_1.default();
app.get("/api/airports", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    let airports = yield airportsController.getAirports();
    res.send(airports);
}));
// start the Express server
app.listen(port, () => {
    console.log(`server started at http://localhost:${port}`);
});
//# sourceMappingURL=server.js.map