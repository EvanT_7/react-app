"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const db_config_1 = __importDefault(require("../config/db-config"));
class AirportsRepository {
    constructor() {
        this.getConnection = () => __awaiter(this, void 0, void 0, function* () {
            this.connection = yield (0, db_config_1.default)();
        });
        this.getAirports = () => __awaiter(this, void 0, void 0, function* () {
            // connection.query('SELECT * FROM airports', (err: any, rows: any) => {
            //     if(err) throw err;
            //     console.log('Data received from Db:');
            //     console.log(rows);
            //     return rows;
            //   });
            try {
                const connection = yield (0, db_config_1.default)();
                let [rows, fields] = yield connection.execute('SELECT * FROM airports');
                return rows;
            }
            catch (e) {
                console.log(e);
            }
        });
    }
}
exports.default = AirportsRepository;
//# sourceMappingURL=airports-repository.js.map