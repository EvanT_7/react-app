import AirportsRepository from '../repositories/airports-repository';

const airportsRepo = new AirportsRepository();

class AirportsController {

    public getAirports = async () => {
        let airports = await airportsRepo.getAirports()
        return airports;
        return {"airports": airports};
    }
   
  }

export default AirportsController;