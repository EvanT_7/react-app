require('dotenv').config();

const mysql = require('mysql2/promise');

const createConnection = async () => {
    const connection = await mysql.createConnection({
        host: process.env.DB_HOST,
        user: process.env.DB_USER,
        password: process.env.DB_PASS,
        database: process.env.DB_NAME
      });
      return connection;
}

//  const connection = mysql.createConnection({
//   host: process.env.DB_HOST,
//   user: process.env.DB_USER,
//   password: process.env.DB_PASS,
//   database: process.env.DB_NAME
// });

// connection.connect((err: any) => {
//   if (err) throw err;
//   console.log('Connected!');
// });

// const connection = await mysql.createConnection({host:'localhost', user: 'root', database: 'test'});


// module.exports = { connection };
export default createConnection;