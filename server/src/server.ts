import express from "express";
// import connection from './config/db-config';
import AirportsController from './controllers/airports-controller';
require('dotenv').config();

const app = express();
const port = process.env.SERVER_PORT; // default port to listen

const data = {
    "data": "Data from the server!"
};

const jokes = {
    "jokes": ["Joke1", "Joke2", "Joke3", "Joke4", "Joke5"]
}

// define a route handler for the default home page
app.get( "/api", ( req, res ) => {
    res.send( "Home page" );
} );

app.get( "/api/data", ( req, res ) => {
    res.send( data );
} );

app.get( "/api/jokes", ( req, res ) => {
    res.send( jokes );
} );

const airportsController = new AirportsController();
app.get( "/api/airports", async ( req, res ) => {
    let airports = await airportsController.getAirports();
    res.send( airports );
} );

// start the Express server
app.listen( port, () => {
    console.log( `server started at http://localhost:${ port }` );
} );