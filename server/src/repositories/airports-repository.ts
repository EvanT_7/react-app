import createConnection from '../config/db-config';


class AirportsRepository {
    connection: any;
    private getConnection = async () => {
        this.connection = await createConnection();
    }

    public getAirports = async () => {
        // connection.query('SELECT * FROM airports', (err: any, rows: any) => {
        //     if(err) throw err;
        //     console.log('Data received from Db:');
        //     console.log(rows);
        //     return rows;
        //   });

        try {
            const connection = await createConnection();
            let [rows, fields] = await connection.execute('SELECT * FROM airports');
            return rows;
        } catch (e) {
            console.log(e);
        }
    }
   

  }

export default AirportsRepository;